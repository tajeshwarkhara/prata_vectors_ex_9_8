#include <iostream>
#include <vector>
#include <string>
using namespace std;

struct Review
{
	string title;
	int rating;
};

bool FillReview(Review& rr);
void showReview(const Review& rr);


int main()
{
	vector<Review> books;
	Review temp;

	while(FillReview(temp))
	{
		books.push_back(temp);
	}

	int num = books.size();

	if(num > 0)
	{
		cout << "Thank you. You entered the following: \n"
			 << "Rating\tBook\n";
		for (int i = 0; i < num; ++i) {
			showReview(books[i]);
		}
		cout << "Reprising:\n"
			 << "Rating\tBook\n";
		vector<Review>::iterator pr;
		for (pr = books.begin(); pr != books.end(); pr++) {
			showReview(*pr);
		}

		// Copy constructor used
		vector<Review> oldlist(books);

		if(num > 3)
		{
			// Remove 2 items
			books.erase(books.begin() + 1, books.begin() + 3);
			cout << "After erasure:\n";
			for(pr = books.begin(); pr != books.end(); pr++)
			{
				showReview(*pr);
			}

			// Insert 1 item
			books.insert(books.begin(), oldlist.begin() + 1, oldlist.begin() + 2);
			cout << "After insertion:\n";
			for(pr = books.begin(); pr != books.end(); pr++)
			{
				showReview(*pr);
			}

			books.swap(oldlist);

			cout << "Swapping oldlist with books:\n";
			for(pr = books.begin(); pr != books.end(); pr++)
			{
				showReview(*pr);
			}
		}
		else
		{
			cout << "Nothing entered, nothing gained.\n";
		}
	}


	return 0;
}

bool FillReview(Review& rr)
{
	cout << "Enter book title (quit to quit): ";
	getline(cin, rr.title);
	if(rr.title == "quit")
	{
		return false;
	}

	cout << "Enter book rating: ";
	cin >> rr.rating;
	if(!cin)
	{
		return false;
	}

	// get rid of rest of the input line
	while(cin.get() != '\n')
	{
		continue;
	}

	return true;
}

void showReview(const Review& rr)
{
	cout << rr.rating << "\t" << rr.title << endl;
}